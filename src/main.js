const join = document.getElementById('join');

document.body.onload = () => {
  join.style.backgroundImage = 'linear-gradient(to top, rgba(65, 65, 65, 0.5), rgba(65, 65, 65, 0.5)), url(./assets/images/joinourprogram.png)';
  join.style.height = "436px";
  join.style.display = 'flex';
  join.style.justifyContent = 'center';
  join.insertAdjacentHTML('afterbegin', '<h2 class="app-title" id="joinHeading">Join Our Program</h2>');
  join.insertAdjacentHTML('beforeend', '<p id="textunder">Sed do eiusmod tempor incididunt<br> ut labore et dolore magna aliqua.</p>');
  join.insertAdjacentHTML('beforeend', '<div id="rectangle"><input type="text" id="emailInput" placeholder="Email"></div>');
  join.insertAdjacentHTML('beforeend', '<button id="subscribeButton">Subscribe</button>');
 
 

  const joinHeading = document.getElementById('joinHeading');
  joinHeading.style.color = "white";
  joinHeading.style.position = 'absolute';
  joinHeading.style.fontWeight = 700;
  joinHeading.style.marginTop = '87px';

  const joinText = join.querySelector('#textunder');
  textunder.style.position = 'absolute';
  textunder.style.fontFamily = "'Source Sans Pro'";
  textunder.style.fontStyle = 'normal';
  textunder.style.fontWeight = '400';
  textunder.style.fontSize = '24px';
  textunder.style.lineHeight = '32px';
  textunder.style.textAlign = 'center';
  textunder.style.color = 'rgba(255, 255, 255, 0.7)';
  textunder.style.marginTop = '176px';

  const rectangle = join.querySelector('#rectangle');
  rectangle.style.width = '400px';
  rectangle.style.height = '36px';
  rectangle.style.left = '307px';
  rectangle.style.top = '300px';
  rectangle.style.background = 'rgba(255, 255, 255, 0.15)';
  rectangle.style.marginTop = '306px';
  rectangle.style.color = '#FFFFFF';
  rectangle.style.fontFamily = 'Source Sans Pro';
  rectangle.style.fontStyle = 'normal';
  rectangle.style.fontWeight = '400';
  rectangle.style.fontSize = '14px';
  rectangle.style.lineHeight = '18px';
  rectangle.style.letterSpacing = '0.05em';
  rectangle.style.display = 'flex';
  rectangle.style.alignItems = 'center';
  rectangle.style.left = '100px';

  const emailInput = join.querySelector('#emailInput');
  emailInput.style.border = 'none';
  emailInput.style.flex = '1';
  emailInput.style.margin = '0';
  emailInput.style.padding = '0 10px';
  emailInput.style.background = 'transparent';
  emailInput.style.color = '#FFFFFF';
  emailInput.style.fontFamily = 'Source Sans Pro';
  emailInput.style.fontStyle = 'normal';
  emailInput.style.fontWeight = '400';
  emailInput.style.fontSize = '14px';
  emailInput.style.lineHeight = '18px';
  emailInput.style.letterSpacing = '0.05em';

 
  

  const subscribeButton = join.querySelector('#subscribeButton');
  if (subscribeButton) {
    subscribeButton.style.width = '111px';
    subscribeButton.style.height = '36px';
    subscribeButton.style.marginLeft = '30px';
    subscribeButton.style.marginTop = '306px';
    subscribeButton.style.background = '#55C2D8'; 
    subscribeButton.style.borderRadius = '18px';
    subscribeButton.style.color = '#FFFFFF';
    subscribeButton.style.fontFamily = 'Oswald';
    subscribeButton.style.fontStyle = 'normal';
    subscribeButton.style.fontWeight = '400';
    subscribeButton.style.fontSize = '16px';
    subscribeButton.style.lineHeight = '26px';
    subscribeButton.style.textAlign = 'center';
    subscribeButton.style.letterSpacing = '0.1em';
    subscribeButton.style.border = 'none';
    subscribeButton.style.cursor = 'pointer';

    subscribeButton.addEventListener('click', (event) => {
      event.preventDefault();
      const emailInput = join.querySelector('#emailInput');
      console.log(emailInput.value);
      emailInput.value = '';
    });
  }

  
  
  
  


};
