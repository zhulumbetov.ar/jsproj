import joinUsSection from './join-us-section.js';

document.addEventListener('DOMContentLoaded', () => {
  joinUsSection.createJoinUsSection();
});